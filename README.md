# Import Calc LibreOffice

Plugin Name:  MyPlugin  
Description:  Plugin para Wordpress para importar archivo Calc de LibreOffice a MariaDB.  
Plugin URI:   https://gitlab.com/georgeguitar/importar_calc.git  
Author:       Juan Dirceu Navarro Arias  
Version:      1.0  
Text Domain:  myplugin  
License:      GPL v2 or later  
License URI:  https://www.gnu.org/licenses/gpl-2.0.txt  


## Descripción:

Plugin para Wordpress para importar archivo Calc de LibreOffice a MariaDB basado en el la funcionalidad parseOds del proyecto ods-php.


## Instalación:

- Para instalar el plugin simplemente se debe activarlo.
- Se incluye un archivo de ejemplo para probar: Ejemplo_calc.ods

## Modo de empleo:  
- Para utilizar el plugin, se debe seleccionar un archivo creado con Calc de LibreOffice. Si no existe errores, el contenido se desplegará en la pantalla, cada hoja (sheet) se crea como una tabla independiente.  
- Aparte de mostrar el conenido del archivo en pantalla, se muestran los scripts generados que serán ejecutados en la base de datos.
- La base de datos empleada es la que se configura con Wordpress. Es posible utilizar otra base de datos de forma independiente utilizando la clase *ImportarDBIndependiente*:  
```
	$importarDB = new ImportarDBIndependiente('localhost', 'usuario', 'pasword', 'DB');
 	$importarDB->importar($stringSql.$stringSQLInsert);
```  
- Los errores se despliegan en la consola "Consola Web" de las herramientas de "Desarrollo" de Firefox.


## Características:  
- El plugin utiliza la funcionalidad *parseOds* del proyecto ods-php (https://sourceforge.net/projects/ods-php/) que descomprime el archivo Calc y convierte en array el XML contenido en *content.xml* que es donde se cuentra la información del archivo Calc.
- El plugin importará archivos Calc de LibreOffice visualizando e importando el contenido de cada hoja (sheet) de Calc en una 
  tablainde pendiente de la DB.
- Los nombres de las tablas empiezan con: *'tabla'* seguido de una numeración que empieza con cero.
- Las columnas tendrán un nombre que empieza con *'columna'* seguido de una numeración que empieza con cero.
```
	CREATE TABLE tabla0  
	  (  
	     columna0 VARCHAR(255),  
	     columna1 VARCHAR(255),  
	     columna2 VARCHAR(255),  
	     columna3 VARCHAR(255),  
	     columna4 VARCHAR(255)  
	  );  
	CREATE TABLE tabla1  
	  (  
	     columna0 VARCHAR(255),  
	     columna1 VARCHAR(255),  
	     columna2 VARCHAR(255)  
	  );  
```

## Limitaciones:  
- El plugin se probó con archivos generados por LibreOffice v.5.2.7.2 pero no debería existir ningún problema para poder trabajar con archivos generados en otras versiones.
- La base de datos no debe contener nombres de tablas que tengan los nombres del tipo: *tabla0, tabla1, tabla2, etc.*
- El archivo Calc, no debe contener hojas vacias.
- Todos los datos se importan como texto con una longitud de 255.
- Solo se acepta archivos con un tamaño máximo de 2097152 bytes.
- Solo se acepta matrices de datos completas.

```
Correcto:
	777	2	90	360	aaaa
	1	2	zzz	360	1440
	2	3	135	540	2160
	
Incorrecto:
	777	2	90		
	1	2	zzz	360	
	2	3	135	540	2160
```

- Formato del script de inserts generados:
```
	INSERT INTO tabla0
	            (columna0,
	             columna1,
	             columna3)
	VALUES      ('aaa',
	             'bbb',
	             'ccc'),
	            ('ddd',
	             'eee',
	             'fff'); 
```