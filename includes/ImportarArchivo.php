<?php

class ImportarArchivo {
	private $archivo;
	private $imprimirConsolaWeb;
	
	function __construct($_archivo) {
		$this->archivo = $_archivo;
		$this->imprimirConsolaWeb = new ImprimirConsolaWeb();
	}
	
	private function importarDB($consulta) {
		global $wpdb;
		
		$sqlParts = array_filter(explode(";", $consulta));
		
		foreach ($sqlParts as $indice =>$query) {
			if (strlen(trim($query))!=0) {
				echo '<br>'.$indice.":".$query;
				$result = $wpdb->query($query);
				echo '<div>';
				if ($result == false)
					echo 'Courrió un error al ejecutar las consultas en la DB: '.$query;
					elseif ($result == 0 )
					echo '¡Sin novedad!.';
					else
						echo 'Se añadíó con éxito las consulta: '.$indice;
						echo '</div>';
			}
		}
	}
	
	private function importarDBIndependiente($urldb, $usuario, $pwdb, $nombredb, $consulta) {
		$conn = new mysqli($urldb, $usuario, $pwdb, $nombredb);
		if ($conn->connect_error) {
			$msdError = 'Falló al conectar a MySQL: ';
			$this->imprimirConsolaWeb->debugToConsole($msdError. $mysqli->connect_error);
		}
		if ($conn->multi_query($consulta) === TRUE) {
			$msg = "¡Consulta ejecutada con éxito!";
			$this->imprimirConsolaWeb->debugToConsole($msg);
			echo '<br>'.$msg.'<br>';
			
		} else {
			$msg = "Error al ejecutar la consulta: ";
			$this->imprimirConsolaWeb->debugToConsole($msg.clean($conn->error));
			echo $msg.$conn->error;
		}
		
		$this->imprimirConsolaWeb.debugToConsole($consulta);
		$conn->close();
	}
	
	private function verificarFilasVacias($rowContent) {
		$cantQueSirven = 0;
		foreach($rowContent as $cellIndex => $cellContent) {
			if (isset($cellContent['value'])) {
				$cantQueSirven++;
				break;
			}
		}
		return $cantQueSirven;
	}
	
	public function importar() {
		$object=parseOds($this->archivo); //Método usado del proyecto ods-php para obtener un objeto del xml contenido en el archivo Calc.
		
		$stringHtml = '';
		$stringSql = '';
		$stringSQLInsert = '';
		$banderaValues = 1;
		foreach ($object->sheets as $tableIndex => $tableContent) {
			$stringHtml .= '<br>Hoja: '.$tableIndex.'<br>';
			$stringHtml .= '<table name="' . $tableIndex . '" style=" border-collapse: collapse; border: 1px solid black;">';
			$stringSql .= 'CREATE TABLE tabla' . $tableIndex . ' (';
			$stringSQLInsert .= 'INSERT INTO tabla' . $tableIndex . ' (';
			$bandera = 1;
			
			foreach ($tableContent['rows'] as $rowIndex => $rowContent) {
				$stringHtml .= '<tr style="border: 1px solid black;">';

				if ($this->verificarFilasVacias($rowContent) > 0) {
					$datosInsert = ' (';
					foreach($rowContent as $cellIndex => $cellContent) {
						if (isset($cellContent['value'])) {
							$stringHtml .= '<td style="border: 1px solid black;">';
							
							if ($bandera == 1) {
								$stringSql .= 'columna'.$cellIndex.' varchar(255),';
								$stringSQLInsert .= 'columna'.$cellIndex.',';
							}
							
							$stringHtml .= '<p>' . $cellContent['value'] . '</p>'; //VALOR DE LA CELDA
							// 					$datosInsert .= '\'' . $cellContent['value'] . '\',';
							$datosInsert .= '"' . $cellContent['value'] . '",';
						}
						$stringHtml .= '</td>';
					}
					$bandera = 0; //Solo se captura la primera fila para los nombres de las columnas
					$stringHtml .= '</tr>';
					
					$stringSQLInsert = trim($stringSQLInsert, ',');
					
					if ($banderaValues == 1) {
						$stringSQLInsert .= ') ';
						$stringSQLInsert .= ' VALUES ';
						$banderaValues = 0;
					} else {
						$stringSQLInsert .= '), ';
					}
					$stringSQLInsert .= $datosInsert;
				}
			}
			$stringHtml .= '</table>';
			$stringSql = trim($stringSql, ',');
			$stringSql .= '); ';
			$banderaValues = 1;
			
			$stringSQLInsert = trim($stringSQLInsert, ',');
			$stringSQLInsert .= '); ';
		}
		print '<br>'.$stringHtml.'<br>';
		
// 		$this->importarDBIndependiente('localhost', 'usuario', 'pasword', 'DB', $stringSql.$stringSQLInsert);
		$this->importarDB($stringSql.$stringSQLInsert);
	}
}