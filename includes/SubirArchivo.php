<?php

class SubirArchivo {
	private $directorio;
	private $archivo;
	private $imprimirConsolaWeb;
	
	function __construct($_directorio) {
		$this->directorio = $_directorio;
		$this->imprimirConsolaWeb = new ImprimirConsolaWeb();
	}

	public function subirArchivo() {
		if(isset($_FILES['archivo_calc'])){
			$errors= "";
			$file_name = $_FILES['archivo_calc']['name'];
			$destino=$this->directorio.$file_name;
			$file_size =$_FILES['archivo_calc']['size'];
			$file_tmp =$_FILES['archivo_calc']['tmp_name'];
			$file_type=$_FILES['archivo_calc']['type'];
	
			$tmp = explode('.', $file_name);
			$file_ext = strtolower(end($tmp));
			
			$expensions= array("ods");
			
			if(in_array($file_ext,$expensions)=== false){
				$errors .= "Extensión de archivo no aceptada, por favor elija un archivo Calc de LibreOffice.";
			}
			
			if($file_size > 2097152){
				$errors .= 'El archivo no debe exeder los 2 MB';
			}
			
			if(empty($errors)==true){
				if(move_uploaded_file($file_tmp,$destino)==true){
					$msg = "¡Archivo subido con éxito! <br>";
					echo $msg;
					$this->imprimirConsolaWeb->debugToConsole($msg);
				}else{
					$msg = "Error, no se pudo subir el archivo <br>";
					echo $msg;
					$this->imprimirConsolaWeb->debugToConsole($msg);
					$destino = "";
				}
			}else{
				$this->imprimirConsolaWeb->debugToConsole($errors);
				print ($errors);
				$destino = "";
			}
			$this->archivo = $destino;
		} else {
			$this->archivo = "";
		}
	}
	
	public function getNombreArchivo() {
		return $this->archivo;
	}
}
?>
