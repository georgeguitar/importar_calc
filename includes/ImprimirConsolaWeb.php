<?php

class ImprimirConsolaWeb {
	
	public function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}
	
	
	public function debugToConsole($data) {
		if(is_array($data) || is_object($data))
		{
			echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
		} else {
			echo("<script>console.log('PHP: ".$data."');</script>");
		}
	}
}

?>