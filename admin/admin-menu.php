<?php

// deshabilita acceso directo a archivo
if ( ! defined( 'ABSPATH' ) ) {
	
	exit;
	
}

/*
 Añadiendo el menú y las configuraciones de página
 */

// añade top-level menu
function import_calc_add_toplevel_menu() {
	
	add_menu_page(
			'Importar datos de Calc',
			'Importar Calc',
			'manage_options',
			'security-example-sanitization',
			'import_calc_display_settings_page',
			'dashicons-admin-generic',
			null
			);
}
add_action( 'admin_menu', 'import_calc_add_toplevel_menu' );
