<?php // MyPlugin - Settings Page

// deshabilito acceso directo a archivos
if ( ! defined( 'ABSPATH' ) ) {

	exit;

}

// muestra el formulario de configuración
function mostrar_formulario() {

	if ( ! current_user_can( 'manage_options' ) ) return;
	
	?>
		<form method="post" enctype="multipart/form-data">
		    <input type="file" name="archivo_calc" />
		    <input type="submit"/>
		 </form>
	<?php

}
