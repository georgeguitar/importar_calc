<?php
/*
Plugin Name: Importar Calc
Description: Plugin para importar datos de um archivo Calc de LibreOffice.  
Plugin URI:  https://gitlab.com/georgeguitar/importar_calc.git  
Author:      Juan Dirceu Navarro Arias  
Version:     1.0  
License:     GPLv2 or later     
License URI: https://www.gnu.org/licenses/gpl-2.0.txt     
*/

// se incluyen las dependencias para el administrador
if ( is_admin() ) { //Solo si el usuariio es administrador
	require_once plugin_dir_path( __FILE__ ) . 'admin/admin-menu.php';
	require_once plugin_dir_path( __FILE__ ) . 'admin/settings-page.php';
}

require_once plugin_dir_path( __FILE__ ) . 'includes/ImprimirConsolaWeb.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/SubirArchivo.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/ImportarArchivo.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/ods.php';

// Despliega la configuración de la página
function import_calc_display_settings_page() {

	// check if user is allowed access
	if ( ! current_user_can( 'manage_options' ) ) return;

	?>

	<div class="wrap">

		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

		<?php mostrar_formulario(); ?>
  		<?php include "importar.php" ?> 

	</div>

<?php

}
