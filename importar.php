<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	$tmp = get_tmp_dir();
	$uid = uniqid();
	$dir = $tmp.'/'.$uid;

	$subirArchivo = new SubirArchivo($dir);
	$subirArchivo->subirArchivo();
	$archivo = $subirArchivo->getNombreArchivo();
	$importarArchivo = new ImportarArchivo($archivo);
	
	if (strlen($archivo)==0) {
		$imprimirConsolaWeb = new ImprimirConsolaWeb();
		$imprimirConsolaWeb->debugToConsole('Archivo no existe!');
	} else {
		$importarArchivo->importar();
	}

?>
